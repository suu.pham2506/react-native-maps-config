import React, { Component } from 'react';
import {
  StyleSheet,
  PermissionsAndroid,
  Platform,
  View,
  Animated,
  Dimensions,
  Image
} from 'react-native';
import { connect } from 'react-redux';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import { isEqual } from 'lodash';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

import Container from '../../components/Container';
import { Note } from '../../components/Text';
import Touchable from '../../components/Touchable';

const GEOLOCATION_OPTIONS = {
  enableHighAccuracy: true,
  timeout: 20000,
  maximumAge: 1000
};

const Images = [
  { uri: 'https://i.imgur.com/sNam9iJ.jpg' },
  { uri: 'https://i.imgur.com/N7rlQYt.jpg' },
  { uri: 'https://i.imgur.com/UDrH0wm.jpg' },
  { uri: 'https://i.imgur.com/Ka8kNST.jpg' }
];

const { width, height } = Dimensions.get('window');

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;

class Login extends Component {
  static navigatorStyle = {
    navBarHidden: true
  };

  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      myPosition: null,
      markers: [
        {
          coordinate: {
            latitude: 16.082450336206268,
            longitude: 108.24219770729543
          },
          title: 'Công viên',
          description: 'Công viên Biển Đông',
          image: Images[0]
        },
        {
          coordinate: {
            latitude: 16.10003791987071,
            longitude: 108.22959501296283
          },
          title: 'Giáo xứ',
          description: 'Giáo xứ Đà Nẵng',
          image: Images[1]
        },
        {
          coordinate: {
            latitude: 16.10167656935111,
            longitude: 108.24983395636083
          },
          title: 'Bảo tàng',
          description: 'Bảo tàng điêu khắc chăm',
          image: Images[2]
        },
        {
          coordinate: {
            latitude: 16.05787549556267,
            longitude: 108.24092768132687
          },
          title: 'Vòng quay',
          description: 'Vòng quay mặt trời',
          image: Images[3]
        }
      ]
    };
  }

  componentWillMount() {
    this.index = 0;
    this.animation = new Animated.Value(0);
  }

  componentDidMount() {
    this.mounted = true;
    if (this.props.coordinate) return;

    if (Platform.OS === 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      ).then(granted => {
        if (granted && this.mounted) this.watchLocation();
      });
    } else {
      this.watchLocation();
    }

    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
      if (index >= this.state.markers.length) {
        index = this.state.markers.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }

      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          const { coordinate } = this.state.markers[index];
          this.customMap.animateToRegion(
            {
              ...coordinate,
              latitudeDelta: 0.04864195044303443,
              longitudeDelta: 0.040142817690068
            },
            350
          );
        }
      }, 10);
    });
  }

  watchLocation() {
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        const myLastPosition = this.state.myPosition;
        const myPosition = position.coords;
        if (!isEqual(myPosition, myLastPosition)) {
          this.setState({ myPosition });
        }
      },
      null,
      this.props.geolocationOptions
    );
  }
  componentWillUnmount() {
    this.mounted = false;
    if (this.watchID) navigator.geolocation.clearWatch(this.watchID);
  }

  onReset = () => {
    this.customMap.animateToCoordinate(
      {
        latitude: this.state.myPosition.latitude,
        longitude: this.state.myPosition.longitude
      },
      100
    );
  };

  render() {
    let { heading, coordinate } = this.props;
    if (!coordinate) {
      const { myPosition } = this.state;
      if (!myPosition) return null;
      coordinate = myPosition;
      heading = myPosition.heading;
    }

    const rotate =
      typeof heading === 'number' && heading >= 0 ? `${heading}deg` : null;

    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH,
        index * CARD_WIDTH,
        (index + 1) * CARD_WIDTH
      ];
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: 'clamp'
      });
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: 'clamp'
      });
      return { scale, opacity };
    });

    return (
      <Container style={styles.map}>
        <MapView
          ref={customMap => {
            this.customMap = customMap;
          }}
          initialRegion={{
            latitude: this.state.myPosition.latitude,
            longitude: this.state.myPosition.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.08
          }}
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          showsUserLocation
          showsMyLocationButton
          showsTraffic={false}
          cacheEnabled
          paddingAdjustmentBehavior="automatic"
          onPress={e => console.log('e', e.nativeEvent)}
          onPoiClick={e => console.log('poi', e.nativeEvent)}
          // onPanDrag={e => console.log('pandrag', e.nativeEvent)}
        >
          {this.state.markers.map((marker, index) => {
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale
                }
              ]
            };
            const opacityStyle = {
              opacity: interpolations[index].opacity
            };
            return (
              <MapView.Marker
                key={index}
                coordinate={marker.coordinate}
                title={marker.title}
              >
                <Animated.View style={styles.markerWrap}>
                  <Animated.View style={styles.ring}>
                    <View style={styles.marker} />
                  </Animated.View>
                </Animated.View>
              </MapView.Marker>
            );
          })}
        </MapView>
        {/* <Animated.ScrollView
          horizontal
          scrollEventThrottle={16}
          showsHorizontalScrollIndicator={false}
          snapToInterval={CARD_WIDTH}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation
                  }
                }
              }
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
          contentContainerStyle={styles.endPadding}
        >
          {this.state.markers.map((marker, index) => (
            <View style={styles.card} key={index}>
              <Image
                source={marker.image}
                style={styles.cardImage}
                resizeMode="cover"
              />
              <View style={styles.textContent}>
                <Note numberOfLines={1} style={styles.cardtitle}>
                  {marker.title}
                </Note>
                <Note numberOfLines={1} style={styles.cardDescription}>
                  {marker.description}
                </Note>
              </View>
            </View>
          ))}
        </Animated.ScrollView> */}
        {/* {Platform.OS === 'ios' && ( */}
        <Touchable onPress={this.onReset}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 56,
              height: 56,
              backgroundColor: '#fff',
              borderRadius: 28,
              position: 'absolute',
              top: 5,
              // bottom: 5,
              // left: 0,
              right: 5,
              zIndex: 4
            }}
          >
            <Note>Reset</Note>
          </View>
        </Touchable>
        {/* )} */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1
  },
  scrollView: {
    position: 'absolute',
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH
  },
  card: {
    padding: 10,
    elevation: 2,
    backgroundColor: '#FFF',
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: 'hidden'
  },
  cardImage: {
    flex: 3,
    width: '100%',
    height: '100%',
    alignSelf: 'center'
  },
  textContent: {
    flex: 1
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: 'bold'
  },
  cardDescription: {
    fontSize: 12,
    color: '#444'
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  marker: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: 'rgba(255,0,0, 0.9)',
    borderWidth: 1.5,
    borderColor: '#fff'
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: 'rgba(255,0,0, 0.3)',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
