import { get, post, del, put } from './utils';
import { store } from '../Setup';

export async function login(data) {
  return post('/api/token', data);
}

export async function logout() {
  return del('/api/token');
}

export async function register(data) {
  return post('/api/users', data);
}

export async function forgotPassword(data) {
  return post('/api/Users/Password/Forgot', data);
}

export async function getInfor() {
  return get('/api/users/me');
}

export async function changePassword(data) {
  const userId = store.getState().user.data.Id;
  return put(`/api/Users/${userId}/Password/Change`, data);
}

export async function changeUserInfo(data) {
  const userId = store.getState().user.data.Id;
  return put(`/api/Users/${userId}/UpdateProfile`, data);
}

export async function changeUserAvatar(data) {
  const userId = store.getState().user.data.Id;
  return put(`/api/Users/${userId}/UpdateAvatar`, data);
}

export async function getNotifications(url) {
  return get(`/api/usernotifications/${url}`);
}

export async function seeNotifications(data) {
  return put('/api/usernotifications/seen', data);
}

export async function getMembershipHistory() {
  const userId = store.getState().user.data.Id;
  return get(`/api/users/${userId}/memberships`);
}

export async function addDevice(data) {
  const userId = store.getState().user.data.Id;
  return post(`/api/users/${userId}/adddevice`, data);
}

export async function getListRequestBooks() {
  return get('/api/bookrequests?sortDesc=true');
}
