import { get, post, del, put } from './utils';
import { store } from '../Setup';

export async function getMemberShip() {
  return get('/api/Memberships');
}

export async function getMemberShipById(memberShipId) {
  return get(`/api/Memberships/${memberShipId}`);
}

export async function updateMemberShip(data) {
  const userId = store.getState().user.data.Id;
  return put(`/api/Users/${userId}/UpdateMembership`, data);
}

export async function checkMemberShipCode(data) {
  return post('/api/GeneratedMemberships/Check', data);
}

export async function sendRequestMembership(data) {
  return post('/api/membershiprequests', data);
}

export async function getAvailableMembership() {
  const userId = store.getState().user.data.Id;
  return get(`/api/memberships/getmemberships?userId=${userId}`);
}

export async function updateMembershipUsePoint(data) {
  const userId = store.getState().user.data.Id;
  return put(`/api/users/${userId}/updatemembershipusepoint`, data);
}
