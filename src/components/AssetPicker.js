import React from 'react';
import { View, Modal, Dimensions, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { Text } from './Text';
import { Colors } from '../themes';
import Touchable from '../components/Touchable';

const { width } = Dimensions.get('window');
export default class AssetPicker extends React.PureComponent {
  state = {
    options: {
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
      noData: false,
      maxWidth: 800,
    },
    visible: false,
    hideView: false,
  };

  onOpenCamera = () => {
    this.setState({ hideView: false });
    ImagePicker.launchCamera(this.state.options, (response) => {
      this.toogleVisible(false);
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        this.props.onGetImageData(response);
      }
    });
  };

  onOpenLibrary = () => {
    this.setState({ hideView: false });
    ImagePicker.launchImageLibrary(this.state.options, (response) => {
      this.toogleVisible(false);
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        this.props.onGetImageData(response);
      }
    });
  };

  toogleVisible = (state) => {
    this.setState({ visible: !this.state.visible, hideView: state });
  };

  render() {
    return (
      <Touchable style={styles.photo} onPress={() => this.toogleVisible(true)}>
        <View style={styles.photo}>
          <Icon
            name="ic-photo"
            size={24}
            color={Colors.transparent}
          />
          <Modal
            animationType="none"
            transparent
            visible={this.state.visible}
            onRequestClose={() => this.toogleVisible(false)}
          >
            {this.state.hideView && (
              <TouchableWithoutFeedback onPress={() => this.toogleVisible(false)}>
                <View style={styles.fadeContainer}>
                  <View style={styles.buttonsContainer}>
                    <TouchableWithoutFeedback onPress={this.onOpenCamera}>
                      <View style={styles.buttonItem}>
                        <Icon name="ic-photo" size={45} color={Colors.red} style={styles.space} />
                        <Text>MÁY ẢNH</Text>
                      </View>
                    </TouchableWithoutFeedback>
                    <View style={styles.divider} />
                    <TouchableWithoutFeedback onPress={this.onOpenLibrary}>
                      <View style={styles.buttonItem}>
                        <Icon name="ic-upload" size={45} style={styles.space} />
                        <Text>THƯ VIỆN</Text>
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            )}
          </Modal>
        </View>
      </Touchable>
    );
  }
}

const styles = StyleSheet.create({
  fadeContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.fade,
  },
  buttonsContainer: {
    height: 200,
    width: width - 30,
    flexDirection: 'row',
    backgroundColor: Colors.snow,
    borderRadius: 5,
  },
  buttonItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  divider: {
    width: 1,
    backgroundColor: '#e4e4e4',
  },
  space: {
    marginBottom: 5,
  },
  photo: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
