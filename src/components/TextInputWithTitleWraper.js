import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, TextInput, Platform } from 'react-native';
import RCTKeyboardToolbarTextInput from 'react-native-textinput-utils';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/thebook-appicon';
import Touchable from './Touchable';
import { Colors } from '../themes';
import { font } from '../themes/Fonts';
import { Note } from '../components/Text';

const { width } = Dimensions.get('window');

class TextInputWithTitleWraper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false,
    };
  }

  onFocus() {
    this.textInputRef.focus();
  }

  renderTextInput(type, width) {
    if (type !== 'nomal' && Platform.OS === 'ios') {
      return (
        <RCTKeyboardToolbarTextInput
          {...this.props}
          ref={(ref) => {
            this.textInputRef = ref;
          }}
          style={[styles.textInput, { width }]}
          keyboardType="numeric"
          rightButtonText={type}
          underlineColorAndroid="transparent"
          placeholderTextColor={Colors.greyLight}
          secureTextEntry={this.props.secureTextEntry && !this.state.showPassword}
        />
      );
    }
    return (
      <TextInput
        {...this.props}
        ref={(ref) => {
          this.textInputRef = ref;
        }}
        style={[styles.textInput, { width }]}
        underlineColorAndroid="transparent"
        placeholderTextColor={Colors.greyLight}
        secureTextEntry={this.props.secureTextEntry && !this.state.showPassword}
      />
    );
  }

  render() {
    const heightForTextinput = Platform.OS === 'ios' ? 38 : 48;
    let widthOfTextinput = width;
    if (this.props.paddingHorizontalValue) {
      widthOfTextinput -= this.props.paddingHorizontalValue * 2;
    }
    if (this.props.icon) {
      const iconWidth = this.props.size ? this.props.size : 18;
      widthOfTextinput -= iconWidth + 10;
    }
    if (this.props.secureTextEntry) {
      widthOfTextinput -= 40;
    }
    return (
      <View
        style={[
          styles.container,
          { paddingHorizontal: this.props.paddingHorizontalValue },
          this.props.viewStyle,
        ]}
      >
        {!_.isEmpty(this.props.titleTextInput) && (
          <Note style={styles.titleTextInput}>
            {this.props.titleTextInput}{' '}
            {this.props.required ? <Note style={{ color: Colors.red }}>*</Note> : ''}
          </Note>
        )}
        <View
          style={[
            styles.row,
            { height: this.props.height ? this.props.height : heightForTextinput },
          ]}
        >
          <View style={{ justifyContent: 'center' }}>
            {this.props.icon && (
              <Icon
                style={styles.icon}
                name={this.props.icon}
                size={this.props.size ? this.props.size : 16}
                color={this.props.iconColor ? this.props.iconColor : Colors.greyLight}
              />
            )}
          </View>
          {this.renderTextInput(
            this.props.numberTextInput ? this.props.numberTextInput : 'nomal',
            this.props.widthTextInput ? this.props.widthTextInput : widthOfTextinput,
          )}
          {this.props.secureTextEntry && (
            <Touchable onPress={() => this.setState({ showPassword: !this.state.showPassword })}>
              <View
                style={[
                  styles.togglePassword,
                  { height: this.props.height ? this.props.height : heightForTextinput },
                ]}
              >
                <Icon
                  name={this.state.showPassword ? 'ic-show-password' : 'ic-hide-password'}
                  size={20}
                />
              </View>
            </Touchable>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    borderColor: '#e9e9e9',
    borderWidth: 1,
  },
  textInput: {
    fontSize: 15,
    fontFamily: font.note,
    color: Colors.darkGrey,
    paddingLeft: 15,
  },
  icon: {
    marginRight: 10,
  },
  togglePassword: {
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleTextInput: {
    fontSize: 13,
    color: Colors.darkGrey,
    marginBottom: 10,
  },
});

export default TextInputWithTitleWraper;
