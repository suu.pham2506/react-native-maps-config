import React, { Component } from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/thebook-appicon';
import ReviewStar from './ReviewStar';
import Touchable from './Touchable';
import { Colors } from '../themes';
import { formatPrice } from '../utils/formatCurrency';
import { SubNote, XLargeText } from './Text';
import Images from '../themes/Images';
import AppStyle from '../themes/AppStyle';
import Tools from '../utils/Tools';

const { width } = Dimensions.get('window');

class CartItemForFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLoading: true,
    };
  }

  onCheckImageError = () => {
    this.setState({ imageLoading: false });
  };

  render() {
    const { data } = this.props;

    if (data) {
      const authorName = Tools.getAuthorName(data.Authors);
      let imageUrl = '';
      if (
        this.props.data &&
        this.props.data.Medias &&
        this.props.data.Medias[0] &&
        this.props.data.Medias[0].ImageAppUrl
      ) {
        imageUrl = this.props.data.Medias[0].ImageAppUrl;
      }
      return (
        <Touchable onPress={this.props.onPress}>
          <View style={styles.wrapCart}>
            {_.isEmpty(imageUrl) ? (
              <View style={styles.imageView}>
                <Image source={Images.bookCover} style={styles.imageStyle} />
              </View>
            ) : (
              <View style={styles.imageView}>
                <Image
                  source={this.state.imageLoading ? { uri: imageUrl } : Images.bookCover}
                  style={styles.imageStyle}
                  onError={this.onCheckImageError}
                />
              </View>
            )}
            <View style={styles.wrapBookContent}>
              <XLargeText style={styles.titleText} numberOfLines={2}>
                {_.isEmpty(data.Title) ? 'Chưa xác định' : data.Title}
              </XLargeText>
              <SubNote style={styles.authorText} numberOfLines={1}>
                {_.isEmpty(authorName) ? 'Chưa xác định' : authorName}
              </SubNote>
              <View style={AppStyle.horizontal}>
                <ReviewStar
                  numberStart={data.OverallStarRating}
                  numberOfReview={data.TotalReview}
                  size={10}
                  fontSize={10}
                />
                <SubNote style={styles.price} numberOfLines={1}>
                  {data.TotalReview}
                </SubNote>
              </View>
              {!_.isEmpty(data.Content) && (
                <SubNote style={{ color: Colors.grey, paddingVertical: 10 }} numberOfLines={3}>
                  {_.truncate(data.Content, { length: 100 })}
                </SubNote>
              )}
              <View style={[AppStyle.horizontal, { marginTop: !_.isEmpty(data.Content) ? 0 : 5 }]}>
                <View style={[AppStyle.horizontal, AppStyle.center]}>
                  <View style={styles.buttonView}>
                    <Icon name="ic-book-1" color={Colors.secondary} size={15} />
                  </View>
                  <SubNote
                    style={[
                      styles.numberBook,
                      {
                        color:
                          data.Availability && data.Availability > 0 ? Colors.greyLight : '#ff9300',
                      },
                    ]}
                    numberOfLines={1}
                  >
                    {data.Availability && data.Availability > 0
                      ? `${data.Availability} quyển`
                      : 'hết sách'}
                  </SubNote>
                </View>

                <View style={[AppStyle.horizontal, AppStyle.center]}>
                  <Touchable>
                    <View style={styles.buttonView}>
                      <Icon name="ic-price-1" color={Colors.secondary} size={16} />
                    </View>
                  </Touchable>
                  <SubNote style={styles.price} numberOfLines={1}>
                    {formatPrice(data.Price)}
                  </SubNote>
                </View>
              </View>
            </View>
          </View>
        </Touchable>
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  titleText: {
    width: width - 178,
    color: Colors.darkGrey,
    fontSize: 14,
  },
  authorText: {
    width: width - 163,
    color: Colors.greyLight,
    paddingTop: 5,
    paddingBottom: 6,
  },
  imageView: {
    height: 150,
    width: 100,
    borderRadius: 4,
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 18,
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: {
      height: 2,
      width: 3,
    },
    // android
    elevation: 3,
  },
  imageStyle: {
    height: 150,
    width: 100,
    borderRadius: 4,
    resizeMode: 'cover',
  },
  wrapCart: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  wrapBookContent: {
    width: width - 157,
  },
  price: {
    color: Colors.greyLight,
    marginLeft: 7,
  },
  numberBook: {
    marginRight: 16,
    marginLeft: 5,
    color: Colors.greyLight,
  },
  buttonView: {
    marginTop: 4,
  },
});

export default CartItemForFilter;
