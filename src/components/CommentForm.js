import React from 'react';
import { View, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';
import _ from 'lodash';
import { Text, SubNote, Note } from '../components/Text';
import Touchable from '../components/Touchable';
import ReviewStar from '../components/ReviewStar';
import Images from '../themes/Images';
import { Colors } from '../themes';
import { defineDateFromNow } from '../utils/formatDateToVietnamese';

const CommentForm = (props) => {
  const { data } = props;
  return (
    <View style={{ marginVertical: 10 }}>
      <View style={styles.commentContainer}>
        {_.isEmpty(data.UrlImageUser) ? (
          <View style={styles.profileView}>
            <Image source={Images.defaultAvatar} style={styles.profile} />
          </View>
        ) : (
          <View style={styles.profileView}>
            <Image source={{ uri: data.UrlImageUser }} style={styles.profile} />
          </View>
        )}
        <View style={styles.commentSubContainer}>
          <View style={styles.rowBetween}>
            <Text style={styles.commentName} numberOfLines={2}>
              {data.UserName}
            </Text>
            {props.isMyComment ? (
              <View>
                {(props.status === 'success' ||
                  _.isUndefined(props.status) ||
                  _.isNull(props.status)) && (
                  <View style={styles.buttonContainer}>
                    <Touchable
                      onPress={props.onEditComment}
                      style={styles.generalButton}
                      touchableHighlight="transparent"
                    >
                      <View style={styles.generalButtonView}>
                        <Icon name="ic-edit-comment" color={Colors.semiDarkGrey} size={15} />
                      </View>
                    </Touchable>
                    <Touchable
                      onPress={props.onDeleteComment}
                      style={styles.generalButton}
                      touchableHighlight="transparent"
                    >
                      <View style={styles.generalButtonView}>
                        <Icon name="ic-trash" color={Colors.semiDarkGrey} size={15} />
                      </View>
                    </Touchable>
                  </View>
                )}
                {props.status === 'fail' && (
                  <View>
                    <View style={styles.buttonContainer}>
                      <Touchable
                        onPress={props.onResendComment}
                        style={styles.generalButton}
                        touchableHighlight="transparent"
                      >
                        <View style={styles.generalButtonView}>
                          <Icon name="ic-edit-comment" color={Colors.red} size={15} />
                        </View>
                      </Touchable>
                      <Touchable
                        onPress={props.onDeleteComment}
                        style={styles.generalButton}
                        touchableHighlight="transparent"
                      >
                        <View style={styles.generalButtonView}>
                          <Icon name="ic-trash" color={Colors.red} size={15} />
                        </View>
                      </Touchable>
                    </View>
                    <Note style={styles.error}>Lỗi</Note>
                  </View>
                )}
                {props.status === 'pending' && <ActivityIndicator />}
              </View>
            ) : (
              <SubNote style={styles.commentTime}>{defineDateFromNow(data.CreatedAt)}</SubNote>
            )}
          </View>
          <ReviewStar numberStart={data.StarRating} size={12} />
        </View>
      </View>
      <Note style={styles.commentContent}>{data.Content}</Note>
    </View>
  );
};

const styles = StyleSheet.create({
  commentContainer: {
    flexDirection: 'row',
  },
  profileView: {
    height: 35,
    width: 35,
    borderRadius: 17.5,
  },
  profile: {
    resizeMode: 'cover',
    height: 35,
    width: 35,
    borderRadius: 17.5,
  },
  commentSubContainer: {
    flex: 1,
    marginLeft: 10,
  },
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 1,
    flex: 1,
  },
  commentName: {
    color: Colors.darkGrey,
    fontSize: 16,
    width: 160,
  },
  generalButton: {
    height: 30,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  generalButtonView: {
    height: 30,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  commentTime: {
    color: Colors.greyLight,
    fontSize: 12,
    flex: 1,
    textAlign: 'right',
  },
  commentContent: {
    color: Colors.grey,
    paddingLeft: 45,
    paddingTop: 6,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  error: {
    color: Colors.red,
    alignSelf: 'flex-end',
    paddingRight: 20,
  },
});

export default CommentForm;
