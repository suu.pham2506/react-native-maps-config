import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { formatPrice } from '../utils/formatCurrency';
import { Colors } from '../themes';
import { Note, XLargeText } from './Text';
import Touchable from './Touchable';
import ReviewStar from './ReviewStar';
import Images from '../themes/Images';

class BookView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLoading: true,
    };
  }

  onCheckImageError = () => {
    this.setState({ imageLoading: false });
  };

  render() {
    let authorsName = '';
    let imageUrl = '';
    if (
      this.props.data &&
      this.props.data.Medias &&
      this.props.data.Medias[0] &&
      this.props.data.Medias[0].ImageAppUrl
    ) {
      imageUrl = this.props.data.Medias[0].ImageAppUrl;
    }
    if (this.props.data.Authors && this.props.data.Authors.length > 0) {
      _.map(this.props.data.Authors, (item) => {
        if (_.isEmpty(authorsName)) {
          authorsName = item.Name;
        } else {
          authorsName = `${authorsName}, ${item.Name}`;
        }
      });
    }
    return (
      <Touchable onPress={() => this.props.onPressDetail(this.props.data)}>
        <View style={this.props.customize ? styles.containerCustom : styles.container}>
          {_.isEmpty(imageUrl) ? (
            <View style={this.props.customize ? styles.imageViewCustom : styles.imageView}>
              <Image
                source={Images.bookCover}
                style={this.props.customize ? styles.imageStyleCustom : styles.imageStyle}
              />
            </View>
          ) : (
            <View style={this.props.customize ? styles.imageViewCustom : styles.imageView}>
              <Image
                source={this.state.imageLoading ? { uri: imageUrl } : Images.bookCover}
                style={this.props.customize ? styles.imageStyleCustom : styles.imageStyle}
                onError={this.onCheckImageError}
              />
            </View>
          )}

          <XLargeText
            style={this.props.customize ? styles.titleTextCustom : styles.titleText}
            numberOfLines={1}
          >
            {this.props.data.Title}
          </XLargeText>
          <Note
            style={this.props.customize ? styles.authorTextCustom : styles.authorText}
            numberOfLines={1}
          >
            {_.isEmpty(authorsName) ? 'Chưa xác định' : authorsName}
          </Note>
          {this.props.custom ? (
            <View style={{ flexDirection: 'row' }}>
              <View style={styles.availability}>
                <Icon name="ic-book-1" color={Colors.secondary} size={15} />
                <Note
                  style={{
                    color: this.props.data.Availability === 0 ? Colors.yellow : Colors.greyLight,
                    marginLeft: 7,
                  }}
                >
                  {this.props.data.Availability}
                </Note>
              </View>
              <View style={styles.price}>
                <Icon
                  name="ic-price-1"
                  color={Colors.secondary}
                  size={16}
                  style={{ marginTop: 3 }}
                />
                <Note style={{ color: Colors.grey, fontSize: 15, marginLeft: 7 }}>
                  {formatPrice(this.props.data.Price)}
                </Note>
              </View>
            </View>
          ) : (
            <ReviewStar
              numberStart={this.props.data.OverallStarRating}
              numberOfReview={this.props.data.TotalReview}
              showTotalReview
            />
          )}
        </View>
      </Touchable>
    );
  }
}

export default BookView;

const styles = StyleSheet.create({
  container: { width: 150 },
  containerCustom: { width: 100 },
  imageView: {
    height: 219,
    width: 150,
    borderRadius: 2,
    borderColor: 'white',
    borderWidth: 0.5,
    shadowOpacity: 0.2,
    shadowRadius: 3,
    backgroundColor: 'white',
    shadowColor: 'rgba(0, 0, 0, 0.70)',
    shadowOffset: {
      height: 2,
      width: 3,
    },
    elevation: 3,
  },
  imageViewCustom: {
    height: 140,
    width: 100,
    borderRadius: 2,
    borderColor: 'white',
    borderWidth: 0.5,
    shadowOpacity: 0.2,
    shadowRadius: 3,
    backgroundColor: 'white',
    shadowColor: 'rgba(0, 0, 0, 0.70)',
    shadowOffset: {
      height: 2,
      width: 3,
    },
    elevation: 3,
  },
  titleText: {
    marginTop: 10,
    color: Colors.darkGrey,
  },
  titleTextCustom: {
    marginTop: 10,
    color: 'black',
    fontSize: 12,
  },
  authorText: {
    color: Colors.greyLight,
    paddingTop: 1,
    paddingBottom: 3,
  },
  authorTextCustom: {
    color: '#464646',
    paddingTop: 1,
    paddingBottom: 3,
    fontSize: 10,
  },
  imageStyle: {
    resizeMode: 'cover',
    width: 150,
    height: 219,
    borderRadius: 2,
  },
  imageStyleCustom: {
    resizeMode: 'cover',
    width: 100,
    height: 140,
    borderRadius: 2,
  },
  availability: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  price: {
    flexDirection: 'row',
    marginLeft: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
