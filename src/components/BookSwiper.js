import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import _ from 'lodash';
// import Carousel from 'react-native-snap-carousel';
import Touchable from './Touchable';
import BookView from './BookView';
import { Text } from './Text';
import { Colors } from '../themes';

const { width } = Dimensions.get('window');

class BookSwiper extends Component {
  renderItem = ({ item }) => {
    return (
      <View style={styles.bookHomeItem}>
        <BookView data={item} onPressDetail={data => this.props.onPressDetail(data)} />
      </View>
    );
  };
  render() {
    const newData = _.filter(this.props.data, item => !item.IsDeleted);
    return (
      <View style={styles.container}>
        <View style={styles.topView}>
          <Text style={styles.titleSwiper}>{this.props.title}</Text>
          <Touchable onPress={this.props.onViewAll}>
            <View style={{ height: 33 }}>
              <Text style={styles.seeAll}>xem hết</Text>
            </View>
          </Touchable>
        </View>
        <Carousel
          data={newData}
          renderItem={this.renderItem}
          sliderWidth={width}
          itemWidth={170}
          firstItem={0}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          enableMomentum={false}
          containerCustomStyle={{ flex: 1 }}
          activeSlideAlignment="start"
          loop
        />
      </View>
    );
  }
}

export default BookSwiper;

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    height: 325,
    marginBottom: 5,
  },
  topView: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
  bookHomeItem: {
    width: 170,
    justifyContent: 'center',
    paddingLeft: 20,
  },
  seeAll: {
    color: Colors.primary,
    marginTop: 5,
  },
  titleSwiper: { fontSize: 21, color: '#262626' },
});
