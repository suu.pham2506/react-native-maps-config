import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, TextInput, Platform } from 'react-native';
import { RCTKeyboardToolbarTextInput } from 'react-native-textinput-utils';
import { Colors, Fonts } from '../themes';
import { font } from '../themes/Fonts';
import Icon from 'react-native-vector-icons/nenapp-iconfont';

const { width } = Dimensions.get('window');
const TEXT_IMPUT = 'TEXT_INPUT';

class TextInputWithIcon extends Component {
  onFocus() {
    this.refs.TEXT_INPUT.focus();
  }

  renderTextInput(type) {
    if (type !== 'nomal' && Platform.OS === 'ios') {
      return (
        // <RCTKeyboardToolbarTextInput
        //   {...this.props}
        //   ref={TEXT_IMPUT}
        //   style={[
        //     styles.textInput,
        //     { color: this.props.textColor ? this.props.textColor : 'black' },
        //   ]}
        //   keyboardType="numeric"
        //   rightButtonText={type}
        //   placeholderTextColor="white"
        // />
      );
    }
    return (
      <TextInput
        {...this.props}
        ref={TEXT_IMPUT}
        style={[styles.textInput, { color: this.props.textColor ? this.props.textColor : 'black' }]}
        underlineColorAndroid="transparent"
        placeholderTextColor="white"
      />
    );
  }

  render() {
    return (
      <View style={[styles.container, this.props.style && this.props.style]}>
        <View style={styles.row}>
          {this.props.icon && (
            <View style={{ justifyContent: 'center' }}>
              <Icon
                style={styles.icon}
                name={this.props.icon}
                size={this.props.size ? this.props.size : 25}
                color={this.props.iconColor ? this.props.iconColor : 'black'}
              />
            </View>
          )}
          {this.renderTextInput(this.props.numberTextInput ? this.props.numberTextInput : 'nomal')}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderRadius: 25,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: Colors.fade,
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  icon: {
    marginHorizontal: 25,
  },
  textInput: {
    flex: 1,
    fontSize: 16,
    fontFamily: font.text,
  },
});

export default TextInputWithIcon;
