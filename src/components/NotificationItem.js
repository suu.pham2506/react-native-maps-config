import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { XLargeText, SubNote, Note } from '../components/Text';
import Colors from '../themes/Colors';

const { width } = Dimensions.get('window');

const Notification = (props) => {
  return (
    <View style={{ backgroundColor: props.isSeen ? '#fff' : '#f5f5f5' }}>
      <View style={styles.contentView}>
        <View style={[styles.iconView, { backgroundColor: props.backgroundColor }]}>
          <Icon name={props.icon} color={props.iconColor ? props.iconColor : '#fff'} size={22} />
        </View>
        <View>
          <View style={styles.textView}>
            <XLargeText style={{ color: Colors.darkGrey }} numberOfLines={1}>
              {props.notificationTitle}
            </XLargeText>
          </View>
          <View style={styles.descriptionView}>
            <Note style={styles.descriptionText}>{props.description}</Note>
          </View>
          <SubNote style={styles.timeText}>{props.time}</SubNote>
        </View>
      </View>
    </View>
  );
};

export default Notification;

const styles = StyleSheet.create({
  contentView: {
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 15,
    marginVertical: 20,
  },
  timeText: {
    color: Colors.greyLight,
    fontSize: 13,
    paddingRight: 10,
    paddingTop: 10,
  },
  textView: {
    width: width - 65 - 23,
  },
  iconView: {
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  descriptionView: {
    width: width - 65,
    paddingTop: 6,
  },
  descriptionText: {
    color: Colors.grey,
    textAlign: 'justify',
    paddingRight: 25,
  },
});
