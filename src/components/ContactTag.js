import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';
import Touchable from '../components/Touchable';
import { Colors } from '../themes';

const { width } = Dimensions.get('window');

const ContactTag = (props) => {
  return (
    <View style={styles.contactTag}>
      <View style={styles.wrapContent}>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.content}>{props.content}</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.divider} />
        <Touchable onPress={props.onPress}>
          <View style={[styles.socialButton, { marginRight: 10, marginLeft: 8 }]}>
            <Icon name={props.iconName} color={props.iconColor} size={18} />
          </View>
        </Touchable>
      </View>
    </View>
  );
};

export default ContactTag;

const styles = StyleSheet.create({
  contactTag: {
    flexDirection: 'row',
    height: 55,
    width: width - 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.default,
    marginTop: 15,
    borderRadius: 4,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 2,
      width: 2,
    },
    // android
    elevation: 5,
  },
  title: {
    fontSize: 10,
    color: '#4ec4d1',
  },
  wrapContent: { marginLeft: 15 },
  content: {
    fontSize: 12,
    paddingTop: 2,
  },
  divider: {
    width: 1,
    height: 35,
    backgroundColor: '#ededed',
  },
  socialButton: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
