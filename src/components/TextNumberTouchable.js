import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from '../components/Text';
import Touchable from '../components/Touchable';

const TextNumberTouchable = (props) => {
  return (
    <Touchable onPress={props.onPress}>
      <View style={styles.container}>
        <Text style={[styles.textTitle, { color: props.selected ? 'black' : '#ababab' }]}>{props.title}</Text>
        <Text style={[styles.textNumber, { color: props.selected ? 'black' : '#ababab' }]}>{props.number}</Text>
      </View>
    </Touchable>
  );
};

export default TextNumberTouchable;

const styles = StyleSheet.create({
  textTitle: {
    fontSize: 21,
  },
  textNumber: {
    fontSize: 18,
  },
  container: {
    width: 125,
    height: 66,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
