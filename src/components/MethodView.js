import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { Colors } from '../themes';
import { Text } from './Text';

const MethodView = (props) => {
  return (
    <View
      style={[
        styles.wrapContent,
        { borderColor: props.isActive ? Colors.primary : Colors.greyLight },
      ]}
    >
      <Icon
        name={props.iconName}
        color={props.isActive ? Colors.primary : Colors.darkGrey}
        size={30}
      />
      <Text
        style={{
          color: props.isActive ? Colors.primary : Colors.darkGrey,
          fontSize: 11,
          marginTop: 8,
        }}
      >
        {props.textTitle}
      </Text>
    </View>
  );
};

export default MethodView;

const styles = StyleSheet.create({
  wrapContent: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
    borderWidth: 0.5,
    width: 107,
    height: 69,
  },
});
