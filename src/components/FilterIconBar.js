import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';
import Touchable from './Touchable';
import Colors from '../themes/Colors';
import { Note } from './Text';
import AppStyle from '../themes/AppStyle';

const { width } = Dimensions.get('window');

const FilterIconBar = (props) => {
  return (
    <View>
      {props.filterType ? (
        <Touchable onPress={props.onPress}>
          <View style={[AppStyle.horizontal, styles.container]}>
            <Note style={styles.filterType}>{props.filterType}</Note>
            <View style={styles.buttonView}>
              <Icon
                name={props.icon}
                olor={Colors.semiDarkGrey}
                size={16}
                style={{ marginRight: 11 }}
              />
            </View>
          </View>
        </Touchable>
      ) : (
        <Touchable onPress={props.onPress}>
          <View style={styles.buttonContainer}>
            <Icon name={props.icon} color={Colors.semiDarkGrey} size={15} />
          </View>
        </Touchable>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: '#e9e9e9',
    height: 40,
    width: (width - 44) / 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  filterType: {
    color: Colors.darkGrey,
    marginLeft: 20,
  },
  buttonView: {
    marginTop: 4,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 44,
    height: 40,
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: '#e9e9e9',
  },
});

export default FilterIconBar;
