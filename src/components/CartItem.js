import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Image, Platform } from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { Note, LargeText, XLargeText } from './Text';
import { formatPrice } from '../utils/formatCurrency';
import ReviewStar from './ReviewStar';
import Touchable from './Touchable';
import { Colors } from '../themes';
import Images from '../themes/Images';
import Tool from '../utils/Tools';

const { width } = Dimensions.get('window');
class CartItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLoading: true,
    };
  }

  onCheckImageError = () => {
    this.setState({ imageLoading: false });
  };

  render() {
    const { data } = this.props;
    const authorsName = Tool.getAuthorName(data.Book.Authors);
    let imageUrl = '';
    if (data.Book && data.Book.Medias && data.Book.Medias[0] && data.Book.Medias[0].ImageAppUrl) {
      imageUrl = data.Book.Medias[0].ImageAppUrl;
    }
    return (
      <View style={styles.wrapCart}>
        {_.isEmpty(imageUrl) ? (
          <View style={styles.imageView}>
            <Image source={Images.bookCover} style={styles.imageStyle} />
          </View>
        ) : (
          <View style={styles.imageView}>
            <Image
              source={this.state.imageLoading ? { uri: imageUrl } : Images.bookCover}
              style={styles.imageStyle}
              onError={this.onCheckImageError}
            />
          </View>
        )}
        <View style={styles.wrapBookContent}>
          <XLargeText style={styles.titleText} numberOfLines={1}>
            {data.Book.Title}
          </XLargeText>
          <Note style={styles.authorText} numberOfLines={1}>
            {_.isEmpty(authorsName) ? 'Chưa xác định' : authorsName}
          </Note>
          <ReviewStar
            numberOfReview
            numberStart={data.Book.OverallStarRating}
            size={12}
            style={styles.star}
          />
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Touchable>
                <View style={styles.buttonView}>
                  <Icon name="ic-price-1" color={Colors.yellow} size={16} />
                </View>
              </Touchable>
              <Note style={styles.price} numberOfLines={1}>
                {formatPrice(data.Book.Price * data.Quantity)}
              </Note>
            </View>
          </View>
        </View>
        <Touchable onPress={this.props.onDelete} style={styles.closeView}>
          <View style={styles.closeView}>
            <Icon name="ic-delete" size={10} color={Colors.grey} />
          </View>
        </Touchable>
        <View style={styles.quantityView}>
          <Touchable
            onPress={() => this.props.onUpdateQuantity(data.Quantity - 1)}
            style={styles.buttonContainer}
          >
            <View style={styles.buttonView1}>
              <Icon name="ic-subtract" size={12} color={Colors.grey} />
            </View>
          </Touchable>
          <Touchable style={styles.buttonContainer}>
            <View style={styles.buttonView1}>
              <LargeText style={styles.quantity} numberOfLines={1}>
                {data.Quantity}
              </LargeText>
            </View>
          </Touchable>
          <Touchable
            onPress={() => this.props.onUpdateQuantity(data.Quantity + 1)}
            style={styles.buttonContainer}
          >
            <View style={styles.buttonView1}>
              <Icon name="ic-add" size={12} color={Colors.grey} />
            </View>
          </Touchable>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleText: {
    width: width - 180,
    color: Colors.darkGrey,
  },
  authorText: {
    width: width - 180,
    color: Colors.greyLight,
    paddingTop: 1,
    paddingBottom: 5,
  },
  imageView: {
    height: 151,
    width: 103,
    marginHorizontal: 15,
    borderRadius: 4,
    borderColor: 'transparent',
    borderWidth: 0.5,
    backgroundColor: 'white',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: {
      height: 2,
      width: 3,
    },
    // android
    elevation: 3,
  },
  imageStyle: {
    height: 151,
    width: 103,
    borderRadius: 4,
    resizeMode: 'cover',
  },
  wrapCart: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingBottom: 2,
  },
  wrapBookContent: {
    width: width - 190,
    justifyContent: 'center',
  },
  star: {
    marginBottom: 20,
  },
  price: {
    color: Colors.greyLight,
    marginLeft: 8,
  },
  quantity: {
    color: Colors.lightD,
    fontSize: 15,
  },
  closeView: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonView: {
    marginTop: 4,
  },
  buttonContainer: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonView1: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  quantityView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? 20 : 15,
    right: Platform.OS === 'ios' ? 25 : 30,
  },
});

export default CartItem;
