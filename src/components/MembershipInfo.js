import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from '../components/Text';

const MembershipInfo = (props) => {
  return (
    <View
      style={[styles.textContainer, { marginBottom: props.marginBottom ? props.marginBottom : 0 }]}
    >
      <Text style={styles.title}>{props.title}</Text>
      <Text style={styles.value}>{props.value}</Text>
    </View>
  );
};

export default MembershipInfo;

const styles = StyleSheet.create({
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 12,
  },
  title: {
    color: '#757575',
    fontSize: 10,
    marginLeft: 25,
  },
  value: {
    color: '#000',
    fontFamily: 'SVN-ProximaNovaSemiBold',
    fontSize: 12,
    marginRight: 25,
  },
});
