import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, TextInput, Platform } from 'react-native';
import RCTKeyboardToolbarTextInput from 'react-native-textinput-utils';
import Icon from 'react-native-vector-icons/thebook-appicon';
import Touchable from './Touchable';
import { Colors } from '../themes';
import { font } from '../themes/Fonts';

const { width } = Dimensions.get('window');

class TextInputWithUnderLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false,
    };
  }

  onFocus() {
    this.textInputRef.focus();
  }

  renderTextInput(type, width) {
    if (type !== 'nomal' && Platform.OS === 'ios') {
      return (
        <RCTKeyboardToolbarTextInput
          {...this.props}
          ref={(ref) => {
            this.textInputRef = ref;
          }}
          style={[styles.textInput, { width }]}
          keyboardType="numeric"
          rightButtonText={type}
          underlineColorAndroid="transparent"
          placeholderTextColor={Colors.greyLight}
          secureTextEntry={this.props.secureTextEntry && !this.state.showPassword}
        />
      );
    }
    return (
      <TextInput
        {...this.props}
        ref={(ref) => {
          this.textInputRef = ref;
        }}
        style={[styles.textInput, { width }]}
        underlineColorAndroid="transparent"
        placeholderTextColor={Colors.greyLight}
        secureTextEntry={this.props.secureTextEntry && !this.state.showPassword}
      />
    );
  }

  render() {
    let widthOfTextinput = width;
    if (this.props.paddingHorizontalValue) {
      widthOfTextinput -= this.props.paddingHorizontalValue * 2;
    }
    if (this.props.icon) {
      const iconWidth = this.props.size ? this.props.size : 18;
      widthOfTextinput -= iconWidth + 10;
    }
    if (this.props.secureTextEntry) {
      widthOfTextinput -= 30;
    }
    return (
      <View
        style={[
          styles.container,
          { paddingHorizontal: this.props.paddingHorizontalValue },
          this.props.viewStyle,
        ]}
      >
        <View style={styles.row}>
          <View style={{ justifyContent: 'center' }}>
            {this.props.icon && (
              <Icon
                style={styles.icon}
                name={this.props.icon}
                size={this.props.size ? this.props.size : 16}
                color={this.props.iconColor ? this.props.iconColor : Colors.greyLight}
              />
            )}
          </View>
          {this.renderTextInput(
            this.props.numberTextInput ? this.props.numberTextInput : 'nomal',
            widthOfTextinput,
          )}
          {this.props.secureTextEntry && (
            <Touchable onPress={() => this.setState({ showPassword: !this.state.showPassword })}>
              <View
                style={styles.togglePassword}
              >
                <Icon
                  name={this.state.showPassword ? 'ic-hide-password' : 'ic-show-password'}
                  size={20}
                />
              </View>
            </Touchable>
          )}
        </View>
        <View
          style={[
            styles.separator,
            { backgroundColor: this.props.colorBorder ? this.props.colorBorder : Colors.divider },
          ]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    height: Platform.OS === 'ios' ? 35 : 45,
  },
  textInput: {
    fontSize: 18,
    fontFamily: font.note,
    color: Colors.semiDarkGrey,

  },
  separator: {
    height: 0.5,
    backgroundColor: Colors.divider,
    marginTop: Platform.OS === 'ios' ? 2 : 0,
  },
  icon: {
    marginRight: 10,
  },
  togglePassword: {
    width: 40,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default TextInputWithUnderLine;
