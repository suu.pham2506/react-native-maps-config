import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { SubText } from '../components/Text';
import { Colors } from '../themes';
import Touchable from '../components/Touchable';

const InfoTag = (props) => {
  return (
    <View style={[styles.container, props.style]}>
      <Touchable onPress={props.onPress ? props.onPress : null}>
        <View style={styles.iconContainer}>
          <Icon name={props.iconName} color={Colors.primary} size={16} />
        </View>
      </Touchable>
      <View style={styles.textContainer}>
        <SubText style={styles.title}>{props.title}</SubText>
        <SubText style={styles.content}>{props.content}</SubText>
      </View>
    </View>
  );
};

export default InfoTag;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    marginLeft: 5,
  },
  title: {
    color: Colors.darkGrey,
    fontSize: 11,
    marginBottom: 2,
  },
  content: {
    color: '#1b969f',
    fontSize: 13,
    fontFamily: 'SVN-ProximaNovaSemiBold',
  },
  iconContainer: {
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
