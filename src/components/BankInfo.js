import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Note, SubNote } from './Text';
import { Colors } from '../themes';

const BankInfo = (props) => {
  return (
    <View style={styles.bankContainer}>
      <View>
        <Image style={styles.logo} source={props.image} resizeMode="cover" />
      </View>
      <View style={styles.textContainer}>
        <Note style={styles.code}>{props.bankCode}</Note>
        <SubNote style={styles.detailText}>{props.detailText}</SubNote>
      </View>
    </View>
  );
};

export default BankInfo;

const styles = StyleSheet.create({
  bankContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  logo: {
    width: 30,
    height: 30,
  },
  textContainer: {
    marginLeft: 18,
  },
  detailText: {
    marginTop: 5,
    fontSize: 13,
    color: Colors.grey,
  },
  code: {
    color: '#262626',
  },
});
