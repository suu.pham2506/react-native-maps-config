import React from 'react';
import { View, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { font } from '../themes/Fonts';
import { Note } from '../components/Text';
import { Colors } from '../themes';

const { height, width } = Dimensions.get('window');
const ratioHeight = height / 667;
const ratioWidth = width / 335;

const TouchableOpacityWithRectangle = (props) => {
  if (props.custom) {
    return (
      <View style={{ marginLeft: props.marginLeft }}>
        <TouchableOpacity
          {...props}
          style={[
            styles.button,
            {
              backgroundColor: 'white',
              borderWidth: 0.5,
              borderColor: props.backgroundColor,
              width: props.width ? props.width * ratioWidth : 122 * ratioWidth,
              height: props.height ? props.height : 40 * ratioHeight,
              marginTop: props.marginTop,
            },
          ]}
        >
          <Note style={{ color: Colors.grey, fontFamily: font.text }}>{props.buttonName}</Note>
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View style={{ marginLeft: props.marginLeft }}>
      <TouchableOpacity
        {...props}
        style={[
          styles.button,
          {
            backgroundColor: props.backgroundColor,
            marginBottom: props.marginBottom,
            marginTop: props.marginTop,
            width: props.width ? props.width * ratioWidth : 122 * ratioWidth,
            height: props.height ? props.height : 40 * ratioHeight,
          },
        ]}
      >
        <Note
          style={{
            fontSize: props.fontSize ? props.fontSize : 15,
            fontFamily: font.header,
            color: Colors.default,
          }}
        >
          {props.buttonName}
        </Note>
      </TouchableOpacity>
    </View>
  );
};

export default TouchableOpacityWithRectangle;

const styles = StyleSheet.create({
  button: {
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
