import React from 'react';
import { View, StyleSheet } from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/thebook-appicon';
import { Colors } from '../themes';
import { SubNote } from '../components/Text';

const ReviewStar = (props) => {
  const { size, numberStart, numberOfReview } = props;
  return (
    <View style={[props.style && props.style, { flexDirection: 'row', alignItems: 'center' }]}>
      <Icon
        name="ic-star"
        size={size || 10}
        color={_.round(numberStart) > 0 ? Colors.yellow : Colors.light}
      />
      <Icon
        name="ic-star"
        size={size || 10}
        color={_.round(numberStart) > 1 ? Colors.yellow : Colors.light}
      />
      <Icon
        name="ic-star"
        size={size || 10}
        color={_.round(numberStart) > 2 ? Colors.yellow : Colors.light}
      />
      <Icon
        name="ic-star"
        size={size || 10}
        color={_.round(numberStart) > 3 ? Colors.yellow : Colors.light}
      />
      <Icon
        name="ic-star"
        size={size || 10}
        color={_.round(numberStart) > 4 ? Colors.yellow : Colors.light}
      />
      {props.showTotalReview && (
        <SubNote
          style={[styles.numberOfReview, { fontSize: props.fontSize ? props.fontSize : 13 }]}
        >
          {numberOfReview}
        </SubNote>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  numberOfReview: {
    color: Colors.greyLight,
    marginLeft: 10,
  },
});

export default ReviewStar;
