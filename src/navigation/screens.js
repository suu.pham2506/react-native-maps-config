import { Navigation } from 'react-native-navigation';
import Login from '../screens/User/Login';

export function registerScreens(store, Provider) {
  Navigation.registerComponent('login', () => Login, store, Provider);
}
