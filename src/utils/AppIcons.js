import { PixelRatio } from 'react-native';
import Icon from 'react-native-vector-icons/thebook-appicon';

const navIconSize = __DEV__ === false ? PixelRatio.getPixelSizeForLayoutSize(40) : 40; // eslint-disable-line
const replaceSuffixPattern = /--(active|big|small|very-big)/g;
const icons = {
  'ic-home': [23, '#000'],
  'ic-cart': [20, '#000'],
  'ic-notification-1': [23, '#000'],
  'ic-library': [23, '#000'],
  'ic-back': [20, '#5f5f5f'],
  'ic-search': [18, '#000'],
  'ic-select': [18, '#000'],
  'ic-delete': [16, '#5f5f5f'],
  'ic-reload': [18, '#5f5f5f'],
  'ic-tick': [18, '#000'],
  'ic-user': [23, '#000'],
  'ic-order': [23, '#000'],
  'ic-trash': [18, '#262626'],
  'ic-pencil': [18, '#5f5f5f'],
  'ic-order-1': [23, '#000'],
  'ic-profile-1': [23, '#000'],
  'ic-library-1': [23, '#000'],
};

const iconsMap = {};
const iconsLoaded = new Promise((resolve, reject) => {
  new Promise.all(Object.keys(icons).map(iconName =>
  // IconName--suffix--other-suffix is just the mapping name in iconsMap
    Icon.getImageSource(
      iconName.replace(replaceSuffixPattern, ''),
      icons[iconName][0],
      icons[iconName][1],
    )))
    .then((sources) => {
      Object.keys(icons).forEach((iconName, idx) => (iconsMap[iconName] = sources[idx]));

      // Call resolve (and we are done)
      resolve(true);
    })
    .catch(err => console.log(err));
});

export { iconsMap, iconsLoaded };
