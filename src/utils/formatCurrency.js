export function formatPrice(value) {
  if (value) {
    // value = value.toString();
    // value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }
  return '0';
}
