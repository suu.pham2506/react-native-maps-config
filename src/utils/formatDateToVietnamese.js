import moment from 'moment';
import 'moment/locale/vi';

export function defineDateFromNow(date) {
  moment.locale('vi');
  const now = moment();
  const dateParams = moment(new Date(date));
  const diffDays = now.diff(dateParams, 'days');
  const diffYears = now.diff(dateParams, 'years');
  if (diffDays === 0) {
    return dateParams.fromNow();
  }
  if (diffDays === 1) {
    return dateParams.format('[Hôm qua lúc] HH:mm');
  }
  if (diffYears === 0) {
    return dateParams.format('DD/MM [lúc ]HH:mm');
  }
  return dateParams.format('DD/MM/YYYY');
}
