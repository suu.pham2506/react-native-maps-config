import React from 'react';
import { AsyncStorage, Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
// import codePush from 'react-native-code-push';
import { Colors } from './themes';
import { registerScreens } from './navigation/screens';
import { iconsMap, iconsLoaded } from './utils/AppIcons';
import LoginActions from '../src/redux/LoginRedux/actions';

// import { iconsMap, iconsLoaded } from './utils/AppIcons';
import configureStore from './redux/store';

export let store = null;

const initialTabIndex = 0;

export const navigatorStyle = {
  navBarBackgroundColor: 'white',
  navBarButtonColor: Colors.darkGrey,
  navBarTextFontFamily: 'System',
  navBarTextFontSize: 18,
  navBarTextColor: '#262626',
  navBarNoBorder: false,
  navBarTitleTextCentered: true,
};

export function push(navigator, config, navigatorButtons, navHidden = false, tabBarHidden = true) {
  navigator.push({
    ...config,
    backButtonTitle: '',
    navigatorButtons: {
      ...navigatorButtons,
      leftButtons: [
        {
          icon: iconsMap['ic-back'],
          id: 'back',
        },
      ],
    },
    navigatorStyle: {
      ...navigatorStyle,
      navBarHidden: navHidden,
      tabBarHidden,
    },
  });
}

export function showModal(navigator, config, navigatorButtons, navHidden) {
  navigator.showModal({
    ...config,
    navigatorButtons: {
      ...navigatorButtons,
      leftButtons: [
        {
          icon: iconsMap['ic-delete'],
          id: 'close',
        },
      ],
    },
    navigatorStyle: { ...navigatorStyle, navBarHidden: navHidden || false },
  });
}

export function startIntroContent() {
  Navigation.startSingleScreenApp({
    screen: {
      screen: 'login',
      navigatorStyle: {
        navBarHidden: true,
      },
    },
    appStyle: {
      orientation: 'portrait',
    },
  });
}

// export function startMainContent() {
//   Navigation.startTabBasedApp({
//     tabs: [
//       {
//         screen: 'dashboard',
//         title: 'Screen 1',
//         icon: iconsMap['ic-home'],
//         iconInsets: {
//           top: 6,
//           left: 0,
//           bottom: -6,
//           right: 0,
//         },
//       },
//       {
//         screen: 'orderList',
//         title: 'Lịch sử đơn hàng',
//         icon: iconsMap['ic-order-1'],
//         iconInsets: {
//           top: 6,
//           left: 0,
//           bottom: -6,
//           right: 0,
//         },
//         navigatorStyle,
//       },
//       {
//         screen: 'userProfile',
//         title: 'Screen 3',
//         icon: iconsMap['ic-profile-1'],
//         iconInsets: {
//           top: 6,
//           left: 0,
//           bottom: -6,
//           right: 0,
//         },
//       },
//       {
//         screen: 'notification',
//         title: 'Thông báo',
//         icon: iconsMap['ic-notification-1'],
//         iconInsets: {
//           top: 6,
//           left: 0,
//           bottom: -6,
//           right: 0,
//         },
//         navigatorStyle,
//       },
//       {
//         screen: 'library',
//         title: 'Screen 5',
//         icon: iconsMap['ic-library-1'],
//         iconInsets: {
//           top: 6,
//           left: 0,
//           bottom: -6,
//           right: 0,
//         },
//       },
//     ],
//     tabsStyle: {
//       initialTabIndex,
//       tabBarSelectedButtonColor: Colors.primary,
//       tabBarBackgroundColor: '#f5f5f5',
//       tabBarButtonColor: '#ababab',
//     },
//     appStyle: {
//       tabBarBackgroundColor: '#f5f5f5',
//       tabBarButtonColor: '#ababab',
//       tabBarSelectedButtonColor: Colors.primary,
//       orientation: 'portrait',
//       navBarTitleFontSize: 18,
//       navBarTitleFontFamily: 'SVN-ProximaNova',
//       navBarTextFontSize: 18,
//       navBarTextFontFamily: 'SVN-ProximaNova',
//       initialTabIndex,
//     },
//     drawer: {
//       left: {
//         screen: 'menuBookFilter',
//       },
//       disableOpenGesture: true,
//       style: {
//         drawerShadow: 'NO',
//         contentOverlayColor: 'rgba(0,0,0,0.55)',
//       },
//     },
//   });
// }

class App extends React.Component {
  constructor(props) {
    super(props);
    this.loadIntial();
  }

  loadIntial() {
    Promise.all([this.loadStore(), iconsLoaded])
      .then(() => {
        this.startApp();
      })
      .catch(err => console.log(err));
  }

  async loadStore() {
    return new Promise((resolve) => {
      store = configureStore(() => resolve('Store loaded'));
      registerScreens(store, Provider);
    });
  }

  startApp() {
    startIntroContent();
  }
}

export default App;
